from selenium import webdriver
import os # For creating/deleting directories
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time

# User defined
from downloadlib import *


game_name = "JewloftheDragon"
desktop_directory = "C:\\Users\\francis_t\\Desktop\\" # The direction of the slashes is important for downloading ...

# Careful with lower/upper case for the Boolean
if (os.path.isdir(desktop_directory + game_name) == False):
    print("Creating directory"+desktop_directory+game_name)
    os.mkdir(desktop_directory + game_name)


prefs = {'download.default_directory' : desktop_directory + game_name}
print("Downloading to" + desktop_directory + game_name)

# Navigate to document download website
# We need to specify a download directory, otherwise it'll download automatically to Downloads folder
# Need to login to document by doing http://username:password@yoursite.com
options = webdriver.ChromeOptions()
options.add_experimental_option('prefs', prefs)
chromepath = r"C:\\Users\\francis_t\\Desktop\\Scripts\\chromedriver.exe"
driver = webdriver.Chrome(chromepath, chrome_options = options)
url = "http://f.tran@gaminglabs.com:Kagdudeguy1234@njintranet5/sites/qms/GLI%20Document%20"+"Library/Forms/Search.aspx"
print(url)
driver.get(url)

driver.implicitly_wait(30)

# Artwork Verification
Download(driver, "fm-en-450")

# Emulation
Download(driver, "fm-en-519")

# Accounting
Download(driver, "fm-en-338")

# Progressive Accounting
# Download(driver, "fm-en-407")

# # Incomplete Games
Download(driver, "fm-en-522")

# # Game Enable/Disable
# Download(driver, "fm-en-523")

# # Game Duration
Download(driver, "fm-en-456")

# # Signature Coversheet
Download(driver, "fm-en-373")

## ------------- FORMS ------------- ##
# FIF Form
Download(driver, "fm-en-001")

# Personality Coversheet
Download(driver, "fm-tc-826")

# Jurisdictional Requirements (for LQ)
Download(driver, "fm-en-395")

# # Europe Letter
Download(driver, "fm-qa-040")

# Italy Letter - uses GLIRA
Download(driver, "ITA - LC330")
## ------------- FORMS ------------- ##

# May we should think about renaming the files
# Prepend them with the game name and underscore. Add a delay to wait for the download to finish
time.sleep(4)
fullpath = desktop_directory + game_name
Prepend_Name(fullpath, game_name + "_")