from selenium import webdriver
import os # For creating/deleting directories
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

# Downloads the form with Document_ID from http://njintranet5/sites/qms/GLI%20Document%20Library/Forms/Search.aspx?FilterName=ISO%5Fx0020%5FID&FilterMultiValue=*fm-en-450*
def Download(driver, Document_ID):
    
    # Fill out the search criteria and submit the form
    searchbar = driver.find_element_by_id("got")
    searchbar.send_keys(Document_ID)
    field = Select(driver.find_element_by_id("list"))
    field.select_by_visible_text('Document ID') 
    Go = driver.find_element_by_xpath("//input[@value='Go']")
    Go.click()

    # Click on the dots, a box should appear
    dots = driver.find_element_by_class_name("ms-list-itemLink")
    dots.click()

    # Click on the download dots
    dots2 = driver.find_element_by_class_name("js-callout-ecbMenu")
    dots2.click()

    # https://support.google.com/chrome/thread/8871110?hl=en -> This solution worked, changed the path to use \\ instead of /
    # WebDriverWait(driver, 20).until(EC.element_to_be_clickable((By.XPATH, "//li[@text='Download a Copy']"))).click()
    download = driver.find_element_by_xpath("//li[@text='Download a Copy']")
    download.click()
    driver.implicitly_wait(10)


# Renames all the files in folder by prepending a prefix
def Prepend_Name(folder, prefix):

    files = os.listdir(folder)

    for file in files:
        print(file)
        os.rename(os.path.join(folder, file), os.path.join(folder, prefix + file)) # rename(src, dst)

