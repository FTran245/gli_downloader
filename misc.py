import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import PyQt5
from PyQt5 import QtWidgets
import json

# Pop up error handling template
class PopUpError(QtWidgets.QDialog):
    def __init__(self, *args, **kwargs):
        super(PopUpError, self).__init__(*args, **kwargs)

        self.setWindowTitle("Error")

        QBtn = QtWidgets.QDialogButtonBox.Ok 
        
        self.buttonBox = QtWidgets.QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

    # This 'self' refers to the self of the pop up box, not the original for we are filling out, it's weird like that
    def errortext(self, text):
        self.label = QtWidgets.QLabel(text)
        self.label.setAlignment(Qt.AlignCenter) # This is a false error, but it aligns your text

        self.layout = QtWidgets.QVBoxLayout()
        self.layout.addWidget(self.label)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

def init_data(formtype, docs):
    
    if (formtype == "testing"):
        FORMS = {}
        for forms in docs["testing"]:
            FORMS[docs["testing"][forms][-10:-1]] = False

        return FORMS
    

def update_data(self, FORMS, formtype):
    if (formtype == "testing"):

        for checkbox in self.testing_checkboxes:
            if (checkbox.isChecked()):
                # print(checkbox.text()[-10:-1])
                FORMS[checkbox.text()[-10:-1]] = True
            else:
                FORMS[checkbox.text()[-10:-1]] = False



# Check to see if no checkboxes are selected - go and throw and error if return is False
def All_Unchecked(FORMS_dict):
    if (all(value == False for value in FORMS_dict.values())):
        return True
    else:
        return False

def read_json(fname):
    f = open(fname, "r")
    data = json.loads(f.read())
    return data
