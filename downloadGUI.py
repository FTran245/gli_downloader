import sys
from PyQt5 import QtCore

# from PyQt5.QtWidgets import QApplication, QLabel # This throws a false error
from PyQt5 import QtWidgets
from downloadGUI_lib import *


# This is to let python know if other scripts are importing this script in their main(), or if this is going to be the main() script
# Let's define our classes and functions in other files
if __name__ =='__main__':
    

    # Lay out your app https://www.learnpyqt.com/courses/start/layouts/ 
    # We'll probably use grid lay out
    # https://build-system.fman.io/pyqt5-tutorial
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle("Fusion")

    UI = Window()
    UI.show()

    
    app.exec_() # Let's the application run until the user closes it
