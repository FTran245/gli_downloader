from selenium import webdriver
import os # For creating/deleting directories
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.action_chains import ActionChains

import time

def close_chromedriver(driver):
    driver.quit()

def init_chromedriver(game_name, desktop_directory, url):
    # desktop_directory = "C:\\Users\\francis_t\\Desktop\\" # The direction of the slashes is important for downloading ...

    prefs = {'download.default_directory' : desktop_directory + game_name, 'plugins.always_open_pdf_externally' : True}

    print("Downloading to " + desktop_directory + game_name)

    # Navigate to document download website
    # We need to specify a download directory, otherwise it'll download automatically to Downloads folder
    # Need to login to document by doing http://username:password@yoursite.com
    options = webdriver.ChromeOptions()
    options.add_experimental_option('prefs', prefs)
    cwd = os.getcwd()
    
    # chromepath = cwd + r"chromedriver.exe"
    chromepath = os.path.join(cwd, "chromedriver.exe")
    driver = webdriver.Chrome(chromepath, chrome_options = options)

    # Maybe implement something to prevent Selenium from just hanging
    # This will resend the .get() method of Selenium 
    finished = False
    while finished == False:
        try:
            driver.get(url)
            finished = True
        except:
            time.sleep(5)

    driver.implicitly_wait(30)

    return driver

# Downloads the form with Document_ID from http://njintranet5/sites/qms/GLI%20Document%20Library/Forms/Search.aspx?FilterName=ISO%5Fx0020%5FID&FilterMultiValue=*fm-en-450*
def download(driver, Document_ID):
    
    # Fill out the search criteria and submit the form
    searchbar = driver.find_element_by_id("got")
    searchbar.send_keys(Document_ID)
    field = Select(driver.find_element_by_id("list"))
    field.select_by_visible_text('Document ID') 
    Go = driver.find_element_by_xpath("//input[@value='Go']")
    Go.click()

    # Click on the dots, a box should appear
    dots = driver.find_element_by_class_name("ms-list-itemLink")
    dots.click()

    # # Click on the download dots
    # dots2 = driver.find_element_by_class_name("js-callout-ecbMenu")
    # dots2.click()
    linc_text = driver.find_element_by_class_name("js-callout-location")
    link = linc_text.get_attribute("value")

    driver.get(link)

    # This was the old method. The new one is much more reliable, requires less "clicking"
    # https://support.google.com/chrome/thread/8871110?hl=en -> This solution worked, changed the path to use \\ instead of /
    # download = driver.find_element_by_xpath("//li[@text='Download a Copy']")
    # download.click()
    driver.implicitly_wait(5)
    time.sleep(1)

# This downloads directly from the link, no need to click on 'Download a Copy'
# A more "correct" solution would be to get the link from the box, but this will be phased out soon anyway so we don't care about this feature
def download_jur(driver, key):
    driver.get(key)
    time.sleep(1)

def init_download(self, gname, savepath, url):
    driver = init_chromedriver(gname, savepath, url)
        
    # Download the checked testing forms
    for key in self.FORMS_testing:
        if(self.FORMS_testing[key] == True):
            # print(key, self.FORMS_testing[key])
            download(driver, key)
            time.sleep(0.8)

    return driver

def init_download_jur(self, gname, savepath, url):
    driver = init_chromedriver(gname, savepath, url)
    
    # parse through list of true/false for all jurisdictional forms, if true, send the index of the document to download_jur()
    for key in self.FORMS_jurisdictional:
        if(self.FORMS_jurisdictional[key] == True):
            download_jur(driver, key)
            time.sleep(1)

    return driver

# Renames all the files in folder by prepending a prefix
def prepend_name(game_name, folder):
    files = os.listdir(folder)

    for file in files:
        print(file)
        os.rename(os.path.join(folder, file), os.path.join(folder, game_name + "_" + file)) # rename(src, dst)


