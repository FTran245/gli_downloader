import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import PyQt5
import time

import pathlib
from shutil import copy

# from PyQt5.QtWidgets import QApplication, QLabel # This throws a false error
from PyQt5 import QtWidgets

from automate_download import *
from misc import *

# For some reason all the class functions need "self" in their argument list... no idea why - it must be a python thing
class Window(QtWidgets.QWidget):

    # Initialize the GUI window, create the grid layout and label the title of the window
    # This is basically like the "main" function of the class - it runs upon initialization
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        self.docs = read_json("testing_forms.json")

        # for jj in self.docs["jurisdictional_WII"]:
        #     print(self.docs["jurisdictional_WII"][jj])

        self.FORMS_testing = init_data("testing", self.docs)    

        grid = self.add_grid() # user defined, creates the upper half of the vbox (vertical box) layout

        self.setLayout(grid)
        self.setWindowTitle("Form Download")
        self.resize(400, 300)

        # Runs .on_click() when you click the dowload button
        self.download_button.clicked.connect(self.on_click)

    def add_grid(self):
        # Create an overall grid for interactive buttons etc..
        grid = QtWidgets.QGridLayout()

        # Populate the grid
        grid.addWidget(self.add_game_name("Game Name:"), 0, 0)

        grid.addWidget(self.add_worksheets_checkbox_list(), 1, 0) # Testing excel worksheets
        

        # Miscellaneous forms
        # grid.addWidget(self.add_text_box("Misc."), 2, 1)
        # Now add the download button, this button should start the download..
        self.download_button = QtWidgets.QPushButton("Download")
        grid.addWidget(self.download_button, 2, 2)

        return grid

    # Creates a vertical lay out, adds checkboxes to them
    # Place the testing forms here (artwork, emulation, etc..)
    def add_worksheets_checkbox_list(self):
        groupBox = QtWidgets.QGroupBox("Testing Forms")
        
        self.testing_checkboxes = []

        for tforms in self.docs["testing"]:
            self.testing_checkboxes.append(QtWidgets.QCheckBox(self.docs["testing"][tforms]))

        # Create the vertical box layout and populate it
        vbox = QtWidgets.QVBoxLayout()

        for ii in self.testing_checkboxes:
            vbox.addWidget(ii)

        vbox.addStretch(1) # Makes the checkboxes closer vertically, which is nicer
        groupBox.setLayout(vbox)

        return groupBox

    
    def add_game_name(self, name):
        groupBox = QtWidgets.QGroupBox(name)

        self.game_name = QtWidgets.QLineEdit()

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.game_name)
        groupBox.setLayout(vbox)

        return groupBox

    def add_text_box(self, name):
        groupBox = QtWidgets.QGroupBox(name)

        self.editbox = QtWidgets.QLineEdit()

        vbox = QtWidgets.QVBoxLayout()
        vbox.addWidget(self.editbox)
        groupBox.setLayout(vbox)

        return groupBox

    # Think about the case when no game name is entered
    # This runs when you click the download button
    def on_click(self):
        
        # Check the checkboxes and update the self object
        # update_data(self, self.FORMS_jurisdictional, "jurisdictional") # only include if you want drafts on the UI too
        update_data(self, self.FORMS_testing, "testing")
        
        if(All_Unchecked(self.FORMS_testing)):
            dlg = PopUpError(self)
            dlg.errortext("Nothing selected")
            dlg.exec()
            return
        else:
            pass

        # Get the game name right away
        if (self.game_name.text() == ""):
            dlg2 = PopUpError(self) #instantiate class PopUpError
            dlg2.errortext("Please enter a game name")
            dlg2.exec() # run the pop up dialog
            return
        else:
            gname = self.game_name.text()

        # Here you'll want to implement a download save dialog thing
        # name = QtGui.QFileDialog.getSaveFileName(self, 'Save File')
        # The direction of the slashes is important for downloading ...
        save_directory = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Folder")) + "/"
        savepath = save_directory.replace("/", "\\") # it's much safer to do this workaround, avoids the escape character problem
        
        # Explicitly create the download directory
        # It's automagically created somehow during Chrome downloading the file, this is just a precaution
        # In case we only need jurisdictional files
        fullpath = savepath + gname

        if (not os.path.exists(fullpath)):
            os.mkdir(fullpath)

        # Only initiate the web driver if we need to download documents from the document library
        # if the checkboxes are empty, DO NOT run the web driver
        if (All_Unchecked(self.FORMS_testing)):
            pass
        else: 
            # http://njintranet5/sites/qms/GLI%20Document%20Library/Forms/Search.aspx original link
            url = "http://f.tran@gaminglabs.com:Kagdudeguy1234@njintranet5/sites/qms/GLI%20Document%20"+"Library/Forms/Search.aspx"
            driver = init_download(self, gname, savepath, url)
            time.sleep(2.5)
            close_chromedriver(driver) 
        


        # Rename the documents
        prepend_name(gname, fullpath)

        


